# Old Hardware video player #

# README #

Have you an old pc/netbook and do you want to use it as videoplayer with an old tv/monitor?  
This script does exactly that!

* Lightweight textual interface (curses) 
* It lists folder video entries and run them using [mplayer](http://www.mplayerhq.hu/design7/dload.html) (default) or another player
* It can be controlled by gamepad (a sort of remote control) 

Tested on emachines e350 with linux........

Version: beta

### How do I get set up? ###

	git clone https://alkatron@bitbucket.org/alkatron/oldhw_vp.git  

### Dependencies ###

* Python 3.6.3
* [inputs](https://pypi.python.org/pypi/inputs)
* [dbus-python (1.2.4)](https://pypi.python.org/pypi/dbus-python/1.2.4)
* [mplayer](http://www.mplayerhq.hu/design7/dload.html) 

### Controls ###
* Gamepad script controls:  
**Y**:Up **A**:Down **B**:Ok **START**:Exit **SELECT**:Shutdown
* Keyboard controls:  
**UP**:Up **DOWN**:Down **RIGHT**:Ok **ESC**:Exit

### Gamepad player controls ###
To use mplayer with gamepad too, it must be compiled with **--enable-joystick**  option.  
Gamepad buttons mapping must be done in ~/.mplayer/input.conf  
Example:  

	JOY_BTN1 vo_fullscreen
	JOY_BTN2 seek +5
	JOY_BTN0 seek -5
	JOY_BTN3 pause
	
	JOY_BTN5 sub_step +1
	JOY_BTN4 sub_step -1
	
	
	JOY_AXIS4_MINUS volume -1
	JOY_AXIS4_PLUS volume 1
	
	JOY_BTN9 quit




To avoid issues on oldpcs I obtained the best performance converting the video (obviously with a modern pc) with this command:  

	ffmpeg -i <source> -sn -vcodec libx264 -crf 23 -preset ultrafast -vf scale=800:-2,format=yuv420p -acodec libfaac  converted.mp4



### Console usage ###
	python3 oldhw_vp.py [options]  

Options:  

-f, --folder     :video folder (required)  
-i, --input      :key=keyboard, gmp=gamepad (opyional default key)

Example:  

	python3 oldhw_vp.py -f /home/jack/video -i gmp

[Contacts](http://alkatron.net/email/)
