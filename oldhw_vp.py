'''
Created on Oct 22, 2017

@author: kooliah
'''
import curses
import os, sys, getopt 
from inputs import DeviceManager
import subprocess
from dbus import SystemBus, Interface
from math import ceil

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

def usage():
    print( read('README.md'))

class alkPlayer:
    PLAYERARGS=['mplayer','-vfm', 'ffmpeg', '-zoom', '-framedrop','-input','js-dev=/dev/input/js1' ]
#    FOLDER='/home/alkatron/shared'
    ESC_KEY = 27
    #INPUT='key' # keyboard
    EXTS=('mp4','mkw','avi')
           
    def __init__(self,fldr,inpt):
        self.screen = curses.initscr()
        curses.noecho()
        curses.cbreak()
        curses.start_color()
        self.input=inpt
        self.screen.keypad( 1 )
        curses.init_pair(1,curses.COLOR_BLACK, curses.COLOR_CYAN)
        self.highlightText = curses.color_pair( 1 )
        self.normalText = curses.A_NORMAL
        #self.screen.border( 0 )
        curses.curs_set( 0 )
        self.max_row = 10 #max number of rows
        self.box = curses.newwin( self.max_row + 2, 64, 5, 8 )
        self.box.box()        
        self.position = 1
        self.page = 1        
        self.pages = 0
        self.nOutputLines=0        
        self.outputLines=[]
        self.outputList=[]
        self.i=0        
        
        self.fldr= fldr
        self.getOutputLines()

    def exit(self):
        """ Terminates the curses application. """
        curses.initscr()
        curses.nocbreak()
        curses.echo()
        curses.endwin()

    def up(self):
        if self.page == 1:
            if self.position > 1:
                self.position = self.position - 1
        else:
            if self.position > ( 1 + ( self.max_row * ( self.page - 1 ) ) ):
                self.position = self.position - 1
            else:
                self.page = self.page - 1
                self.position = self.max_row + ( self.max_row * ( self.page - 1 ) )
    def down(self):
        if self.page == 1:
            if self.position < self.i:
                self.position = self.position + 1
            else:
                if self.pages > 1:
                    self.page = self.page + 1
                    self.position = 1 + ( self.max_row * ( self.page - 1 ) )
        elif self.page == self.pages:
            if self.position < self.nOutputLines:
                self.position = self.position + 1
        else:
            if self.position < self.max_row + ( self.max_row * ( self.page - 1 ) ):
                self.position = self.position + 1
            else:
                self.page = self.page + 1
                self.position = 1 + ( self.max_row * ( self.page - 1 ) )

    def run(self):
        while True:
            self.displayScreen()
            
            if self.input== 'gmp':
                if len(DeviceManager().gamepads)==0:
                    self.exit()
                    print('==========================')
                    print('Error:Gamepad not detected')
                    return
                gmp=DeviceManager().gamepads[0]
                events = gmp.read()                
                for event in events:
                    if event.code == 'BTN_SOUTH' and event.state==1:                
                        self.down()
                    elif event.code == 'BTN_WEST' and event.state==1:                
                        self.up()
                    elif event.code == 'BTN_START' and event.state==1:                
                        self.exit()
                        return
                    elif event.code == 'BTN_EAST' and event.state==1:                
                        self.startAction()
                        break
                    elif event.code == 'BTN_SELECT' and event.state==1:                
                        self.shutdown()
                    elif event.code == 'BTN_TR' and event.state==1:
                        pass                
                    elif event.code == 'BTN_TL' and event.state==1:
                        pass                
                    elif event.code == 'BTN_NORTH' and event.state==1:
                        pass                
            else:
                x = self.screen.getch()
                if x == curses.KEY_DOWN:
                    self.down()
                elif x == curses.KEY_UP:
                    self.up()
                elif x == self.ESC_KEY:
                    self.exit()
                    break
                elif x == curses.KEY_RIGHT:
                    self.startAction()

    def shutdown(self):
        self.exit()
        sys_bus = SystemBus()
        ck_srv = sys_bus.get_object('org.freedesktop.ConsoleKit',
                                        '/org/freedesktop/ConsoleKit/Manager')
        ck_iface = Interface(ck_srv, 'org.freedesktop.ConsoleKit.Manager')
        stop_method = ck_iface.get_dbus_method("Stop")
        stop_method()
        
    def startAction(self):
        if self.position==1: # UP
            self.fldr=os.path.join('/',*self.fldr.split('/')[:-1])
            self.getOutputLines()
            return
        line= self.outputList[self.position-2]
        if line.is_dir():
            # entra
            self.fldr=line.path
            self.highlightLineNum = 0
            self.getOutputLines()
        else:
            # mplayer -vfm ffmpeg -zoom -framedrop -lavdopts lowres=1:fast:skiploopfilter=all kj25.mp4 
            args=self.PLAYERARGS + [line.path ]
            p = subprocess.run(args)
            self.screen.clear()
    def getOutputLines(self):
        self.outputList=sorted([x for x in os.scandir(self.fldr) if (x.name[-3:] in self.EXTS) or x.is_dir() ], key=lambda ntry: (ntry.is_file(),ntry.name) )
        self.outputLines =['===UP==='] + [x.name for x in self.outputList] 
        self.nOutputLines = len(self.outputLines)
        self.pages = int( ceil( self.nOutputLines / self.max_row ) )
        self.position = 1
        self.page = 1        

    def displayScreen(self):
        self.screen.border( 0 )
        self.box.erase()
        self.screen.addstr(2,30,'Alkatron Media Player')
        self.screen.addstr(18,30,self.fldr)
        if self.input=='gmp':
            self.screen.addstr(22,3,'Gamepad controls - Y:Up    A:Down    B:Ok    START:Exit    SELECT:Shutdown')
        else:
            self.screen.addstr(22,7,'Keyboard controls - UP:Up   DOWN:Down   RIGHT:Ok   ESC:Exit')
        self.box.border( 0 )
    
        for i in range( 1 + ( self.max_row * ( self.page - 1 ) ), self.max_row + 1 + ( self.max_row * ( self.page - 1 ) ) ):
            if ( i + ( self.max_row * ( self.page - 1 ) ) == self.position + ( self.max_row * ( self.page - 1 ) ) ):
                self.box.addstr( i - ( self.max_row * ( self.page - 1 ) ), 2, self.outputLines[ i - 1 ], self.highlightText  )
            else:
                self.box.addstr( i - ( self.max_row * ( self.page - 1 ) ), 2, self.outputLines[ i - 1 ], self.normalText if self.outputList[i-2].is_file()==True else curses.A_BOLD)
            if i == self.nOutputLines:
                break
    
        self.screen.refresh()
        self.box.refresh()
        self.i=i
 
    # catch any weird termination situations
    def __del__(self):
        pass
        #self.exit()

def main(argv=['-h']):
    _folder=''
    _input='key'
    try:                                
        opts, args = getopt.getopt(argv, "f:ih", ["folder=",'input=']) 
    except getopt.GetoptError:           
        usage()                          
        sys.exit() 
    if not opts:
        usage()
        sys.exit() 
    for opt, arg in opts: 
        if opt in ('-f', '--folder'):                
            _folder= arg
        elif opt in ('-input', '--input'):
            _input= arg
        elif opt in ('-h'):
            usage()
            sys.exit() 
        else:
            print( "Not a valid option") 
            usage()
            sys.exit() 
    if _folder:
        ih = alkPlayer(_folder, _input)
        ih.run()
    else:
        usage()

     
if __name__ == '__main__':
#     _par=['-s', 20,
#       '-a', 5,
#        ]
#     main(_par)
    
    main(sys.argv[1:])
    
    
